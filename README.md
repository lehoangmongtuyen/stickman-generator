# Setup

## Download and install Miniconda
https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe 

## Setup environment

```
conda create -n stickman python=3.6

conda activate stickman

pip install -r requirements.txt

conda deactivate
```

# How to run

``` 
conda activate stickman

python stickman.py
```