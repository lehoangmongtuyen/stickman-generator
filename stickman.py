import numpy as np
import random

MAX_MULTIPLY = 3
MAX_DIVIDE = 8

def get_input():
	with open('input.txt', 'r') as f:
		n = int(f.readline())
		player_strength = int(f.readline())
		is_plus = np.zeros(n+1, dtype=bool)
		is_minus = np.zeros(n+1, dtype=bool)
		is_multiply = np.zeros(n+1, dtype=bool)
		# Is plus 
		m = int(f.readline())
		for i in range(m):
			is_plus[int(f.readline()) - 1] = True
		# Is minus
		m = int(f.readline())
		for i in range(m):
			is_minus[int(f.readline()) - 1] = True
		# Is multiply
		m = int(f.readline())
		for i in range(m):
			is_multiply[int(f.readline()) - 1] = True
	return n, player_strength, is_plus, is_minus, is_multiply


def main():
	n, player_strength, is_plus, is_minus, is_multiply = get_input()
	enemies_strength = np.zeros(n, dtype=int)
	sum1 = 1
	sum2 = player_strength
	for i in range(n):
		if sum1 + 1 >= sum2:
			sum1 = sum2 - 3
		if is_plus[i]:
			enemies_strength[i] = random.randint(sum1, sum2 * 2)
			sum1 = sum2
			sum2 += enemies_strength[i]
		elif is_minus[i]:
			enemies_strength[i] = random.randint(1, sum2 - 1)
			sum1 = sum2
			sum2 -= enemies_strength[i]
		elif is_multiply[i]:
			enemies_strength[i] = random.randint(1, MAX_MULTIPLY)
			sum1 = sum2 
			sum2 *= enemies_strength[i]
		else:
			enemies_strength[i] = random.randint(sum1 + 1, sum2 - 1)
			sum1 = sum2
			sum2 += enemies_strength[i]
	with open('output.txt', 'w') as f:
		f.write(str([''.join(['+',str(x)]) if is_plus[i] else ''.join(['-',str(x)]) if is_minus[i] else ''.join(['*', str(x)]) if is_multiply[i] else str(x) for i, x in enumerate(enemies_strength)]))
if __name__ == "__main__":
	main()